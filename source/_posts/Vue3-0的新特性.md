---
title: Vue3.0的新特性
date: 2020-07-22
tags:
- Vue3
- Vue
- 前端
categories:
- Vue学习笔记
---

# Vue3.0的新特性

## 源码管理方式
相对于Vue2.x中源码的组织方式，Vue3.0使用了`monorepo`，它将这些模块拆分到不同的package中，每一个package都有它对应的API、类型定义和测试

这样将模块拆分的更加细化，模块更加独立、模块之间的依赖关系也更加明确，开发人员更容易阅读、理解和更改所有块源码，增加了代码的可维护性

## 开发语言支持
Vue2.x使用的是原生JavaScript语言开发，因此对ts的支持并不是很好，在Vue3.0中，完全支持了TypeScript语言，使开发变得更加面向对象，增加了类型检查

## 性能优化
Vue3.0在代码体积减小方面做了以下优化
* 移除了一些冷门的feature
* 引入了tree-shaking技术

**tree-shaking:** 依赖ES2015模块语法的静态结构（import和export），通过编译阶段的静态分析，找到没有引入的模块并打上标记，如果在项目中没有引入`Transition`、`KeepAlive`等组件，那么他们对应的代码就不会被打包，这样就间接达到了减少项目引入的Vue.js包体积的目的

## 数据劫持优化
数据的响应式与双向绑定，一直以来都是Vue最大的优点之一

在Vue1.x和2.x中都是通过`Object.definePrototype`这个API对数据的`getter`和`setter`进行劫持

但是这种方法一直存在着缺点，那就是无法检测数据的添加与删除，因此Vue添加了$set和$delete的方法

为了解决这些问题，Vue3.0中使用了Proxy API进行数据劫持，Proxy API并不能监听到内部深层次的对象变化

因此Vue3.0的解决方法是在getter内部进行递归响应式

## 编译优化

new Vue() => init => $mount => compile => render => vnode => patch => DOM

Vue2.x的版本中，如果需要对有数据绑定的节点进行更新，使用diff算法是需要对所有动静态节点进行遍历，而理想状态是直接更新绑定了数据的节点

Vue3.0通过编译阶段对节点进行分析，编译生成了Block tree（将模板基于动态节点指令切割的嵌套区块，每个区块的内部节点结构是固定的，每个区块只需要一个Array来追踪自身包含的动态节点）

Vue3.0还在编译阶段还进行了对slot的编译优化、事件侦听函数的缓存优化，并且在运行时重写了diff算法

## 语法API优化： Composition API

这里需要分两点来说

### 优化逻辑组织

在Vue2.x中，编写组件的本质就是编写一个“包含了描述组件选项的对象”，它被成为Options API（按照methods, computed、data、props）这些不同的选项进行分类

组件小的时候，这种分类方式一目了然，但是大型组件中会有多个逻辑关注点，当使用Options API时，每一个关注点都有自己的Options

如果需要修改一个逻辑关注点，就需要在单个文件中进行上下切换和寻找

Vue3.0中提供了一种新的API就是Composition API，它会将某个逻辑关注点的相关代码都放在一个函数中，这样我们需要修改一个功能时就不用在文件中跳来跳去了

下面是两种写法的直观对比
```vue
<!--  vue2 -->
<template>
  <div></div>
</template>
<script>
export default {
  data() {
    return {
      a: 1
    };
  },
  computed: {
    b() {
      return a + 1;
    }
  },
  props: {
    c: {
      type: Object,
      default() {
        return {};
      }
    }
  },
  methods: {
    m1() {
      return 'm1';
    }
  }
};
</script>
```

```vue
<!-- vue3 -->
<template>
 <div >
 </div>
</template>

<script lang='ts' setup>
import { ref, onMounted } from 'vue';


const props = defineProps({
  b: [String, Number],
});

const a = ref({});
onMounted(async () => {
  init();
});


const init = () => {
  console.log('init');
}
</script>

<style lang='less' scoped>
</style>
```

### 优化逻辑复用

Vue2.x中我们会使用mixins进行混入复用，但是缺点也很明显，就是每个mixins都会定义自己的props、data这样就很容易导致变量冲突

对组件而言如果模板中使用不在当前组件中定义的变量，就不太容易知道这些变量是在哪里定义的（数据来源不清晰）

Vue3.0中使用Composition API，就很好地解决了mixins的这两个问题

## 引入RFC：使每个版本改动可控

RFC(Request For Comments)为新功能进入框架提供一个一致且受控的路径

在Vue3.0中大规模启用了RFC，可以了解每一个feature采用或被废弃掉的前因后果

## 总结

以上就是Vue3.0相对于Vue2.x的最新特性，最为技术发展的主流方向，Vue3.0虽然距离完全普及应用还有很可观的一段时间，而我们更早地学习Vue3.0的相关知识，就更能提升自己，更能掌握未来技术发展的大方向。



