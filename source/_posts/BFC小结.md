---
title: BFC小结
date: 2020-06-03
tags:
- Css
- 前端
categories:
- Css
---
# BFC小结

之前并没有系统地了解过什么是BFC以及它有什么作用，学习之后才发现以前遇到的很多问题都是可以使用BFC解决的

## 什么是BFC
> W3C对BFC的定义如下： 浮动元素和绝对定位元素，非块级盒子的块级容器（例如 inline-blocks, table-cells, 和 table-captions），以及overflow值不为”visiable”的块级盒子，都会为他们的内容创建新的BFC（Block Fromatting Context， 即块级格式上下文）

我们常说的文档流其实分为定位流、浮动流和普通流三种，而普通流其实就是指**BFC**中的**FC**

**FC**是formating context的缩写，也就是格式化上下文，决定了其子元素如何布局以及与其他元素之间的关系

常见的**FC**有BFC（块级格式化上下文）、IFC（行级格式化上下文）、GFC（网格布局格式化上下文）和FFC（自适应格式化上下文）

**BFC**是通过一些特定的属性设置而使元素脱离普通流的束缚形成完全独立的区域，且内部元素与外部元素不会产生相互影响

## BFC的触发条件

BFC的触发条件有下列几种：
* 根元素，即HTML元素
* 浮动元素： `float` 不为 `none`
* 绝对定位元素：`position` 设置为 `absolute` 或 `fixed`
* 行内块级元素：`display` 设置为 `inline-block`
* 表格单元格：`display` 设置为 `table-cell`
* 表格标题： `display` 设置为 `table-caption`
* `overflow` 值不为 `visible` 的块元素 - 弹性盒元素(flex)
* 网格元素：`display`为`grid` 或 `inline-grid` 元素的直接子元素

## BFC的布局规则
* 内部的Box会在垂直方向一个接一个地进行放置
* 属于同一个BFC的两个Box的margin会发生重叠
* BFC区域不会与float的box重叠
* BFC是一个独立的容器，内部元素不会与外部元素产生相互影响
* 计算BFC高度时浮动元素也会参与计算

## 应用场景

### 解决父元素塌陷问题
```html
<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BFCtest</title> 
    <style type="text/css">
    * {
      margin: 0;
      padding: 0;
    }

    .father {
      border: 5px solid lightskyblue;
    }

    .child {
      width: 100px;
      height: 100px;
      background-color: red;
      }
    </style>
</head>
<body>
  <div class="father">
    <div class="child"></div>
  </div>
</body>
</html>
```
![](https://s1.ax1x.com/2020/05/31/t1DgfA.png)

将内部child设置为浮动
```css
.child {
  width: 100px;
  height: 100px;
  background-color: red;
  float: left;
}
```
![](https://s1.ax1x.com/2020/05/31/t1r9k4.jpg)

此时发现父元素发生了塌陷，这是将父元素设置为BFC这样就解决了父元素塌陷的问题
```css
.father {
  border: 5px solid lightskyblue;
  overflow: hidden;
}
```

### 避免外边距重叠
```html
<html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>同一BFC外边距重叠</title>
  <style type="text/css">
    * {
      margin: 0;
      padding: 0;
    }
    .container {
      background-color: lightskyblue;
      overflow: hidden;
    }

    .inner {
      background-color: coral;
      margin: 10px 0;
      text-align: center;
    }
  </style>
</head>
<body>
<div class="container">
  <div class="inner">1div>
  <div class="inner">2div>
  <div class="inner">3div>
<div>
<body>
<html>
```
![](https://s1.ax1x.com/2020/05/31/t1yrwT.jpg)

我们会发现每两个inner元素之间的间距都是10px，这是因为外层container元素为BFC，所以内部子元素发生了margin重叠，我们BFC内部与外部是不会相互影响的，那么这时我们将内部元素封装进BFC中那么就不会出现margin重叠的情况了
```html
<div class="container">
  <div class="inner">1</div>
  <div class="bfc">
    <div class="inner">2</div>
  </div>
  <div class="inner">3</div>
</div>
```
```css
.bfc {
  overflow: hidden;
}
```
![](https://s1.ax1x.com/2020/05/31/t1cYxs.jpg)

这时就会发现`2`对应的元素上下边距已经不再重合了

## 总结
以上就是我学习BFC的内容，示例来源于 [神三元](http://47.98.159.95/my_blog/css/008.html) 大佬的博客，BFC的概念相对抽象，但是通过这些示例能够清除地了解BFc的重要性，同时我也参考了另外两篇文章 [[布局概念] 关于CSS-BFC深入理解](https://juejin.im/post/5909db2fda2f60005d2093db) 、[学习 BFC (Block Formatting Context)](https://juejin.im/post/59b73d5bf265da064618731d)

## 后话
致力于做一个有灵魂的搬运工！！！！

至此完成了我给自己规定的5日学习目标，明天开始更新算法、Vue源码、Js高级进阶等相关的内容！






